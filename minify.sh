#! /usr/bin/env bash

for f in $( ls src/ ); do
	tr -s '\n\t' ' ' < src/$f > dist/$f
done
